﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmpresaCad.aspx.cs" Inherits="PolliCopy.Web.EmpresaCad" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="sm" runat="server">
    </asp:ScriptManager>
    <table style="width: 100%;" cellspacing="5">
        <tr>
            <td class="tdLeft">
                <label for="<%=txtRazaoSocial.ClientID %>">
                    Razão Social/Nome Fantasia <span class="failureNotification">*</span></label>
            </td>
            <td class="tdRight">
                <asp:TextBox ID="txtRazaoSocial" runat="server" Width="500px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvRazaoSocial" runat="server" ErrorMessage="Preencha o campo Razão Social/Nome Fantasia"
                    ControlToValidate="txtRazaoSocial" CssClass="failureNotification" ValidationGroup="Cadastro"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="tdLeft">
                <label for="<%=txtCep.ClientID %>">
                    CEP <span class="failureNotification">*</span></label>
            </td>
            <td class="tdRight">
                <asp:TextBox ID="txtCep" runat="server" Width="70px"></asp:TextBox>
                <asp:MaskedEditExtender ID="txtCep_Mask" runat="server" Mask="99999-999" MaskType="Number"
                    Enabled="True" TargetControlID="txtCep">
                </asp:MaskedEditExtender>
                <asp:RequiredFieldValidator ID="rfvCep" runat="server" ErrorMessage="Preencha o campo CEP"
                    ControlToValidate="txtCep" CssClass="failureNotification" ValidationGroup="Cadastro"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="tdLeft">
                <label for="<%=txtEndereco.ClientID %>">
                    Endereço <span class="failureNotification">*</span></label>
            </td>
            <td class="tdRight">
                <asp:TextBox ID="txtEndereco" runat="server" Width="500px"></asp:TextBox>
                <asp:AutoCompleteExtender ID="txtEndereco_AutoCompleteExtender" runat="server" DelimiterCharacters=""
                    Enabled="True" TargetControlID="txtEndereco" UseContextKey="True" ServiceMethod="GetCompletionList"
                    ServicePath="Enderecos.svc">
                    <Animations>
                        <OnHide>
                            <FadeOut Duration=".5" Fps="20" />
                        </OnHide>
                    </Animations>
                </asp:AutoCompleteExtender>
                <asp:RequiredFieldValidator ID="rfvEndereco" runat="server" ErrorMessage="Preencha o campo endereço"
                    ControlToValidate="txtEndereco" CssClass="failureNotification" ValidationGroup="Cadastro"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="tdLeft">
                <label for="<%=txtContato.ClientID %>">
                    Contato <span class="failureNotification">*</span></label>
            </td>
            <td class="tdRight">
                <asp:TextBox ID="txtContato" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvContato" runat="server" ErrorMessage="Preencha o campo contato"
                    ControlToValidate="txtContato" CssClass="failureNotification" ValidationGroup="Cadastro"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="tdLeft">
                <label for="<%=txtObservacao.ClientID %>">
                    Observação</label>
            </td>
            <td class="tdRight">
                <asp:TextBox ID="txtObservacao" runat="server" Width="500px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="tdLeft">
                <label for="<%=txtDiaVencimentoFatura.ClientID %>">
                    Dia de vencimento da fatura <span class="failureNotification">*</span></label>
            </td>
            <td class="tdRight">
                Todo dia
                <asp:TextBox ID="txtDiaVencimentoFatura" runat="server" Width="20px"></asp:TextBox>
                <asp:MaskedEditExtender ID="txtDiaVencimentoFatura_Mask" runat="server" DisplayMoney="None"
                    Mask="99" Enabled="True" InputDirection="LeftToRight" MaskType="None" TargetControlID="txtDiaVencimentoFatura"
                    UserDateFormat="None" UserTimeFormat="None">
                </asp:MaskedEditExtender>
                <asp:RequiredFieldValidator ID="rfvDiaVencimentoFatura" runat="server" ErrorMessage="Preencha o campo Dia de vencimento da fatura"
                    ControlToValidate="txtDiaVencimentoFatura" CssClass="failureNotification" ValidationGroup="Cadastro"
                    Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="rvDiaVencimentoFatura" runat="server" ErrorMessage="Preencha um número de dia válido (entre 1 e 31)"
                    ControlToValidate="txtDiaVencimentoFatura" CssClass="failureNotification" ValidationGroup="Cadastro"
                    Display="Dynamic" MinimumValue="1" MaximumValue="31"></asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td class="tdLeft">
                <label for="<%=txtTelefonePrincipal.ClientID %>">
                    Telefone (Principal) <span class="failureNotification">*</span></label>
            </td>
            <td class="tdRight">
                <asp:TextBox ID="txtTelefonePrincipal" runat="server"></asp:TextBox>
                <asp:MaskedEditExtender ID="txtTelefonePrincipal_Mask" runat="server" DisplayMoney="None"
                    Mask="(99)9999-9999" Enabled="True" InputDirection="LeftToRight" MaskType="None"
                    TargetControlID="txtTelefonePrincipal" UserDateFormat="None" UserTimeFormat="None">
                </asp:MaskedEditExtender>
                <asp:RequiredFieldValidator ID="rfvTelefonePrincipal" runat="server" ErrorMessage="Preencha o telefone principal da empresa"
                    ControlToValidate="txtTelefonePrincipal" CssClass="failureNotification" ValidationGroup="Cadastro"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="tdLeft">
                <label for="<%=txtTelefoneContato1.ClientID %>">
                    Telefone para contato (1)</label>
            </td>
            <td class="tdRight">
                <asp:TextBox ID="txtTelefoneContato1" runat="server"></asp:TextBox>
                <asp:MaskedEditExtender ID="txtTelefoneContato1_Mask" runat="server" DisplayMoney="None"
                    Mask="(99)9999-9999" Enabled="True" InputDirection="LeftToRight" MaskType="None"
                    TargetControlID="txtTelefoneContato1" UserDateFormat="None" UserTimeFormat="None">
                </asp:MaskedEditExtender>
            </td>
        </tr>
        <tr>
            <td class="tdLeft">
                <label for="<%=txtTelefoneContato2.ClientID %>">
                    Telefone para contato (2)</label>
            </td>
            <td class="tdRight">
                <asp:TextBox ID="txtTelefoneContato2" runat="server"></asp:TextBox>
                <asp:MaskedEditExtender ID="txtTelefoneContato2_Mask" runat="server" DisplayMoney="None"
                    Mask="(99)9999-9999" Enabled="True" InputDirection="LeftToRight" MaskType="None"
                    TargetControlID="txtTelefoneContato2" UserDateFormat="None" UserTimeFormat="None">
                </asp:MaskedEditExtender>
            </td>
        </tr>
        <tr>
            <td class="tdLeft">
                <label for="<%=txtEmail.ClientID %>">
                    E-mail</label>
            </td>
            <td class="tdRight">
                <asp:TextBox ID="txtEmail" runat="server" Width="250px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Um formato de e-mail inválido foi digitado no campo E-mail"
                    ControlToValidate="txtEmail" CssClass="failureNotification" 
                    ValidationGroup="Cadastro" Display="Dynamic" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="tdLeft">
            </td>
            <td class="tdRight">
            </td>
        </tr>
        <tr>
            <td class="tdLeft">
                <asp:Button ID="btnCadastrar" runat="server" Text="Cadastrar" 
                    ValidationGroup="Cadastro" onclick="btnCadastrar_Click" />
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
