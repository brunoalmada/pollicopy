﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PolliCopy.Presenter.Presentations;
using PolliCopy.Presenter.Interfaces;

namespace PolliCopy.Web
{
    public partial class EmpresaCad : System.Web.UI.Page, IEmpresaCad
    {
        private EmpresaCadPresentation _empresaCadPresentation;

        protected void Page_Load(object sender, EventArgs e)
        {
            _empresaCadPresentation = new EmpresaCadPresentation(this);
            _empresaCadPresentation.Load(IsPostBack);
        }

        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            _empresaCadPresentation.Cadastrar();
        }

        public string[] GetCompletionList(string prefixText, int count)
        {
            string[] _enderecos = new string[]
                                          {
                                              "Calçada das Violetas",
                                              "Calçada das Anêmonas",
                                              "Calçada Flor de Lis",
                                              "Alameda Rio Negro",
                                              "Alameda Araguaia"
                                          };

            var resultado = from item in _enderecos
                            where item.StartsWith(prefixText)
                            select item;

            return resultado.ToArray();
        }

        #region Implementacao da Interface

        public string RazaoSocialNomeEmpresa
        {
            get { return txtRazaoSocial.Text; }
            set { txtRazaoSocial.Text = value; }
        }

        public string Cep
        {
            get { return txtCep.Text; }
            set { txtCep.Text = value; }
        }

        public string Endereco
        {
            get { return txtEndereco.Text; }
            set { txtEndereco.Text = value; }
        }

        public string Contato
        {
            get { return txtContato.Text; }
            set { txtContato.Text = value; }
        }

        public string Observacao
        {
            get { return txtObservacao.Text; }
            set { txtObservacao.Text = value; }
        }

        public int DiaVencimento
        {
            get { return Convert.ToInt32(txtDiaVencimentoFatura.Text); }
            set { txtDiaVencimentoFatura.Text = value.ToString(); }
        }

        public string TelefonePrincipal
        {
            get { return txtTelefonePrincipal.Text; }
            set { txtTelefonePrincipal.Text = value; }
        }

        public string TelefoneContato1
        {
            get { return txtTelefoneContato1.Text; }
            set { txtTelefoneContato1.Text = value; }
        }

        public string TelefoneContato2
        {
            get { return txtTelefoneContato2.Text; }
            set { txtTelefoneContato2.Text = value; }
        }

        public string Email
        {
            get { return txtEmail.Text; }
            set { txtEmail.Text = value; }
        }

        public void Teste()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}