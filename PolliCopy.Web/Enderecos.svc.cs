﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

namespace PolliCopy.Web
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Enderecos
    {
        private string[] _enderecos = new string[]
                                          {
                                              "Calçada das Violetas",
                                              "Calçada das Anêmonas",
                                              "Calçada Flor de Lis",
                                              "Calçada das Hortênsias",
                                              "Calçada das Orquídeas",
                                              "Calçada dos Cravos",
                                              "Calçada Vitória Régia",
                                              "Calçada das Acácias",
                                              "Alameda Rio Negro",
                                              "Alameda Araguaia",
                                              "Alameda Madeira",
                                              "Alameda Cauaxi",
                                              "Alameda Purus",
                                              "Alameda Mamoré",
                                              "Alameda Grajaú",
                                              "Alameda Itapecuru",
                                              "Alameda Amazonas",
                                              "Praça das Hortênsias"
                                          };

        // To use HTTP GET, add [WebGet] attribute. (Default ResponseFormat is WebMessageFormat.Json)
        // To create an operation that returns XML,
        //     add [WebGet(ResponseFormat=WebMessageFormat.Xml)],
        //     and include the following line in the operation body:
        //         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
        
        // Add more operations here and mark them with [OperationContract]
        [OperationContract]
        public string[] GetCompletionList(string prefixText, int count, string contextKey)
        {
            var resultado = from item in _enderecos
                            where item.StartsWith(prefixText)
                            select item;

            return resultado.OrderBy(x => x).ToArray();
        }
    }
}
