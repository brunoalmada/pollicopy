﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolliCopy.Model.Entidades
{
    public class Empresa
    {
        public int Id { get; set; }
        public string RazaoSocial { get; set; }
        public string Cep { get; set; }
        public string Endereco { get; set; }
        public string Contato { get; set; }
        public string Observacao { get; set; }
        public int DiaVencimento { get; set; }
        public string TelefonePrincipal { get; set; }
        public string TelefoneContato1 { get; set; }
        public string TelefoneContato2 { get; set; }
        public string Email { get; set; }
    }
}
