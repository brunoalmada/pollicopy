﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PolliCopy.Model.Entidades;

namespace PolliCopy.Model.Mocks
{
    public static class EmpresaMock
    {
        public static Empresa GetMock()
        {
            return new Empresa()
            {
                RazaoSocial = "PolliCopy",
                Cep = "06140123",
                Endereco = "Calçada das Violetas, 188",
                Contato = "Luiz Polli",
                Observacao = "Emitir Nota Fiscal",
                DiaVencimento = 15,
                TelefonePrincipal = "1188521858",
                TelefoneContato1 = "1146881023",
                TelefoneContato2 = "1142064436",
                Email = "pollicopy@ig.com.br"
            };
        }
    }
}
