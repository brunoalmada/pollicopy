﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolliCopy.Presenter.Interfaces
{
    public interface IEmpresaCad
    {
        string RazaoSocialNomeEmpresa { get; set; }
        string Cep { get; set; }
        string Endereco { get; set; }
        string Contato { get; set; }
        string Observacao { get; set; }
        int DiaVencimento { get; set; }
        string TelefonePrincipal { get; set; }
        string TelefoneContato1 { get; set; }
        string TelefoneContato2 { get; set; }
        string Email { get; set; }

        void Teste();
    }
}
