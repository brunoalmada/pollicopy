﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PolliCopy.Presenter.Interfaces;
using PolliCopy.Model.Models;
using PolliCopy.Model.Entidades;
using PolliCopy.Model.Mocks;

namespace PolliCopy.Presenter.Presentations
{
    public class EmpresaCadPresentation
    {
        private IEmpresaCad _view;
        private EmpresaModel _empresaModel;

        public EmpresaCadPresentation(IEmpresaCad view)
        {
            _view = view;
            _empresaModel = new EmpresaModel();
        }

        public void Load(bool isPostBack)
        {
            if (!isPostBack)
                Bind(EmpresaMock.GetMock());
        }

        public void Cadastrar()
        {
            var teste = Bind();
        }

        /// <summary>
        /// Preenche a View com base em um objeto Empresa informado
        /// </summary>
        /// <param name="empresa">Objeto Empresa que ira preencher a View</param>
        private void Bind(Empresa empresa)
        {
            _view.RazaoSocialNomeEmpresa = empresa.RazaoSocial;
            _view.Cep = empresa.Cep;
            _view.Endereco = empresa.Endereco;
            _view.Contato = empresa.Contato;
            _view.Observacao = empresa.Observacao;
            _view.DiaVencimento = empresa.DiaVencimento;
            _view.TelefonePrincipal = empresa.TelefonePrincipal;
            _view.TelefoneContato1 = empresa.TelefoneContato1;
            _view.TelefoneContato2 = empresa.TelefoneContato2;
            _view.Email = empresa.Email;
        }

        /// <summary>
        /// Preenche um objeto Empresa com as informacoes digitadas na View
        /// </summary>
        /// <returns>Objeto Empresa preenchido com as informacoes da View</returns>
        private Empresa Bind()
        {
            return new Empresa
                              {
                                  RazaoSocial = _view.RazaoSocialNomeEmpresa,
                                  Cep = _view.Cep,
                                  Endereco = _view.Endereco,
                                  Contato = _view.Contato,
                                  Observacao = _view.Observacao,
                                  DiaVencimento = _view.DiaVencimento,
                                  TelefonePrincipal = _view.TelefonePrincipal,
                                  TelefoneContato1 = _view.TelefoneContato1,
                                  TelefoneContato2 = _view.TelefoneContato2,
                                  Email = _view.Email
                              };
        }
    }
}
