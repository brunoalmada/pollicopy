# README #

This is a web application which uses the MVP (Model-View-Presenter) pattern, which I think it is very interesting.

### Who do I talk to? ###

* Bruno Almada
* * brunoha@gmail.com
* * [My LinkedIn profile](http://linkedin.com/in/brunoalmada)
* * [My Xing profile](https://www.xing.com/profile/Bruno_HeitzmannAlmada)
* * [about.me](https://about.me/brunoha)